package com.example.user.testapp.InterfaceProject;

import android.support.annotation.StringRes;

public interface InterfaceView {
    void showText(String string);

    void showMessage(@StringRes int string);

    void showProgress();

    void hideProgress();
}
