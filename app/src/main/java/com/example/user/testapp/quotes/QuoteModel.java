package com.example.user.testapp.quotes;

import com.google.gson.annotations.SerializedName;

public class QuoteModel {
    @SerializedName("id")
    private int id;
    @SerializedName("createdBy")
    private int createdBy;
    @SerializedName("text")
    private String text;

    public void setId(int id) {
        this.id = id;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public String getText() {
        return text;
    }
}


