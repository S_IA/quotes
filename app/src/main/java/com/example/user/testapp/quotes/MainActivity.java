package com.example.user.testapp.quotes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.example.user.testapp.InterfaceProject.InterfaceView;
import com.example.user.testapp.R;
import com.example.user.testapp.detail.DetailActivity;

public class MainActivity extends AppCompatActivity implements InterfaceView {

    ListView mList;
    ArrayAdapter adapter;
    MainPresenter presentere;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mList = findViewById(R.id.ma_listView);
        presentere = new MainPresenter(this);

        presentere.getInitialList();

        adapter = new QuoteAdapter(this, R.layout.row1, presentere.getList(), this);
        mList.setAdapter(adapter);

        mList.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        mList.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mList.getLastVisiblePosition() - mList.getHeaderViewsCount() -
                        mList.getFooterViewsCount()) >= (adapter.getCount() - 1)) {

                    presentere.getResponsAsyn();

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if (loadMore) {


                }
            }
        });


    }


    @Override
    public void showText(String string) {
        Toast toast = Toast.makeText(this, string, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void showMessage(int string) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    /**
     * Добавление элементов
     */
    public void addItemAdapter() {
        final int positionToSave = mList.getFirstVisiblePosition();
        adapter.notifyDataSetChanged();

        mList.post(new Runnable() {

            @Override
            public void run() {
                mList.setSelection(positionToSave);
            }
        });

        mList.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                if (mList.getFirstVisiblePosition() == positionToSave) {
                    mList.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    /**
     * Переход на Detail
     *
     * @param quote
     */
    public void startIntent(QuoteModel quote) {
        intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("id", quote.getId());
        startActivity(intent);
    }
}
