package com.example.user.testapp.InterfaceProject;

public interface InterfacePresenter {

    void runProgressBar();

    void hideProgressBar();

    void getMessage(String string);

    void onDestroy();

}
