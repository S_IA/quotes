package com.example.user.testapp.quotes;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListQuotesModel {
    @SerializedName("ok")
    private boolean ok;
    @SerializedName("data")
    private ArrayList<QuoteModel> data;

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public void setData(ArrayList<QuoteModel> data) {
        this.data = data;
    }

    public ArrayList<QuoteModel> getData() {
        return data;
    }

    public boolean isOk() {
        return ok;
    }
}
