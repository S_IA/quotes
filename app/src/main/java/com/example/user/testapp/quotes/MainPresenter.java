package com.example.user.testapp.quotes;

import com.example.user.testapp.InterfaceProject.InterfacePresenter;
import com.example.user.testapp.Network;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements InterfacePresenter {

    private MainActivity mView;
    private List<ListQuotesModel> listQuotes;
    private List<QuoteModel> quotes;
    private Network network;
    private int i; //offset

    public MainPresenter(MainActivity mView) {
        this.mView = mView;
        network = new Network();
        quotes = new ArrayList<QuoteModel>();
        listQuotes = new ArrayList<ListQuotesModel>();
        i = 0;
    }

    @Override
    public void runProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void getMessage(String string) {
        mView.showText(string);
    }

    @Override
    public void onDestroy() {

    }

    /**
     * Получение первоначального списка цитат
     */
    public void getInitialList() {

        network.preparationForReuest();
        getResponsAsyn();

    }

    /**
     * Ответ от сервера асинхронно
     */
    public void getResponsAsyn() {

        network.getApiService().getList(i + 1, 10).enqueue(new Callback<ListQuotesModel>() {
                                                               @Override
                                                               public void onResponse(Call<ListQuotesModel> call, Response<ListQuotesModel> response) {
                                                                   if (response.isSuccessful()) {

                                                                       listQuotes.add((ListQuotesModel) response.body());
                                                                       getListQuote();
                                                                       mView.addItemAdapter();
                                                                       i = i + 10;
                                                                   }
                                                               }

                                                               @Override
                                                               public void onFailure(Call<ListQuotesModel> call, Throwable t) {
                                                                   mView.showText("Произошла ошибка в получении данных");

                                                               }
                                                           }


        );


    }


    /**
     * Составление списка цитат для адавтера
     */
    public void getListQuote() {

        quotes.clear();
        for (ListQuotesModel l : listQuotes
        ) {
            for (QuoteModel l_model : l.getData()
            ) {
                quotes.add(l_model);
            }
        }
    }


    public List<QuoteModel> getList() {
        return quotes;
    }
}

