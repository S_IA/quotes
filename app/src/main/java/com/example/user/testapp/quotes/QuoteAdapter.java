package com.example.user.testapp.quotes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import com.example.user.testapp.R;



import java.util.List;

public class QuoteAdapter extends ArrayAdapter<QuoteModel> {

    private MainActivity mView;
    private LayoutInflater inflater;
    private List<QuoteModel> quotes;
    private int layout;

    private static class MyViews {
        Button btn1;
        Button btn2;
    }


    public QuoteAdapter(Context context, int resource, List<QuoteModel> quotes, MainActivity mView) {
        super(context, resource, quotes);
        this.quotes = quotes;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
        this.mView = mView;
    }

    @SuppressLint("ResourceType")
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(this.layout, parent, false);

        final MyViews views = new MyViews();
        final QuoteModel quote = quotes.get(position);

        views.btn1 = view.findViewById(R.id.button_1);
        views.btn2 = view.findViewById(R.id.button_2);

        View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mView.startIntent(quote);
            }
        };

        views.btn1.setOnClickListener(mOnClickListener);
        views.btn2.setOnClickListener(mOnClickListener);


        if (quote.getCreatedBy() == 1) {

            Button button = view.findViewById(R.id.button_1);
            button.setText(quote.getText());
            Button button_2 = view.findViewById(R.id.button_2);
            button_2.setVisibility(View.GONE);


        } else {
            Button button = view.findViewById(R.id.button_2);
            button.setText(quote.getText());
            Button button_2 = view.findViewById(R.id.button_1);
            button_2.setVisibility(View.GONE);
        }

        return view;
    }




}
