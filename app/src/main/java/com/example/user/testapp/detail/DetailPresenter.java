package com.example.user.testapp.detail;

import com.example.user.testapp.InterfaceProject.InterfacePresenter;
import com.example.user.testapp.Network;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetailPresenter implements InterfacePresenter {
    private DetailActivity mView;
    private Network network;
    private List<ListDetailModel> detail;
    private int id;
    private ArrayList<TagModel> tags;

    public DetailPresenter(DetailActivity mView, int id) {
        this.mView = mView;
        network = new Network();
        this.id = id;
        detail = new ArrayList<ListDetailModel>();
        tags = new ArrayList<TagModel>();
    }

    @Override
    public void runProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void getMessage(String string) {

    }

    @Override
    public void onDestroy() {

    }

    /**
     * Получение первоначального списка цитат
     */
    public void getInitialList() throws IOException {


        network.preparationForReuest();
        getResponsAsyn();


    }

    public void getResponsAsyn() {


        network.getApiService().getDetail(id).enqueue(new Callback<ListDetailModel>() {
            @Override
            public void onResponse(Call<ListDetailModel> call, Response<ListDetailModel> response) {
                if (response.isSuccessful()) {
                    detail.add(response.body());
                    mView.addItemAdapter();
                }
            }

            @Override
            public void onFailure(Call<ListDetailModel> call, Throwable t) {
                mView.showText("Произошла ошибка в получении данных");
            }
        });
    }

    public List<ListDetailModel> getDetail() {
        return detail;
    }

    public List<TagModel> getList() {

        return tags;
    }
}






