package com.example.user.testapp.InterfaceProject;

import com.example.user.testapp.detail.ListDetailModel;
import com.example.user.testapp.quotes.ListQuotesModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface APIService {
    @GET("/test/list.php")
    Call<ListQuotesModel> getList(@Query("offset") int offset, @Query("limit") int limit);

    @GET("/test/detail.php")
    Call<ListDetailModel> getDetail(@Query("id") int id);


}
