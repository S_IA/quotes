package com.example.user.testapp.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.*;
import com.example.user.testapp.InterfaceProject.InterfaceView;
import com.example.user.testapp.R;
import java.io.IOException;


public class DetailActivity extends AppCompatActivity implements InterfaceView {


    TextView text;
    TextView data;
    ListView listTag;
    DetailPresenter presenter;
    ArrayAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        text = findViewById(R.id.text);
        data = findViewById(R.id.date);
        listTag = findViewById(R.id.list_tag);

        int id = getIntent().getExtras().getInt("id");
        presenter = new DetailPresenter(this, id);
        Bundle arguments = getIntent().getExtras();


        try {
            presenter.getInitialList();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void addItemAdapter() {

        ListDetailModel detailModel = presenter.getDetail().get(0);

        text.setText(detailModel.getData().get(0).getText());
        data.setText(detailModel.getData().get(0).getCreatedAt());

        adapter = new DetailAdapter(this, R.layout.tag, presenter.getDetail().get(0).getData().get(0).getTags(), this);
        listTag.setAdapter(adapter);

    }

    @Override
    public void showText(String string) {

    }

    @Override
    public void showMessage(int string) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
