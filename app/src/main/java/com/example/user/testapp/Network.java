package com.example.user.testapp;

import android.app.Application;
import com.example.user.testapp.InterfaceProject.APIService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network extends Application {

    private static Network mInstance;
    private Retrofit retrofit;
    private static APIService apiService;

    public static Network getInstance() {
        if (mInstance == null) {
            mInstance = new Network();
        }
        return mInstance;
    }

    /**
     * Подготовка запроса
     */
    public void preparationForReuest() {

        retrofit = new Retrofit.Builder()
                .baseUrl("http://ds24.ru")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(APIService.class);

    }


    public static APIService getApiService() {
        return apiService;
    }
}
