package com.example.user.testapp.detail;

import com.google.gson.annotations.SerializedName;

public class TagModel {
@SerializedName("label")
    private String label;
@SerializedName("color")
    private String color;

    public void setColor(String color) {
        this.color = color;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getColor() {
        return color;
    }
}
