package com.example.user.testapp.detail;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailModel {
    @SerializedName("tagList")

    private ArrayList<TagModel> tags;
    @SerializedName("createdAt")

    private String createdAt;
    @SerializedName("id")

    private int id;
    @SerializedName("createdBy")

    private int createdBy;
    @SerializedName("text")

    private String text;


    public void setText(String text) {
        this.text = text;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setTags(ArrayList<TagModel> tags) {
        this.tags = tags;
    }

    public String getText() {
        return text;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public int getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public ArrayList<TagModel> getTags() {
        return tags;
    }
}
