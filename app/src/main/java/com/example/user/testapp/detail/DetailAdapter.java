package com.example.user.testapp.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.user.testapp.R;

import java.util.List;


public class DetailAdapter extends ArrayAdapter<TagModel> {


    private DetailActivity mView;
    private LayoutInflater inflater;
    private List<TagModel> tags;
    private int layout;

    public DetailAdapter(Context context, int resource, List<TagModel> tags,DetailActivity mView) {
        super(context, resource, tags);
        this.tags = tags;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
        this.mView = mView;

    }



    @SuppressLint("ResourceType")
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        TextView tag = view.findViewById(R.id.textView);

      tag.setText(tags.get(position).getLabel());
      if(tags.get(position).getColor() != null){

          int color = Color.parseColor(tags.get(position).getColor());
          tag.setBackgroundColor(color);
      }

        return view;
    }
}
