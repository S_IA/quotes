package com.example.user.testapp.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListDetailModel {
    @SerializedName("ok")
    private boolean ok;
    @SerializedName("data")
    @Expose
    private ArrayList<DetailModel> data;

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public void setData(ArrayList<DetailModel> data) {
        this.data = data;
    }

    public ArrayList<DetailModel> getData() {
        return data;
    }

    public boolean isOk() {
        return ok;
    }


}
